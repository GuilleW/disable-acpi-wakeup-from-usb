# Disable ACPI wakeup from USB

Install this service to disable your computer to wake up from USB devices like mouse and/or keyboard.
The service run the script just before sleep, so it doesn't slow your startup.

## Getting started

### Install
```sh
git clone https://gitlab.com/GuilleW/disable-acpi-wakeup-from-usb.git
cd disable-acpi-wakeup-from-usb
sudo ./install.sh
```
You can remove this folder after install.

### Uninstall
```sh
git clone https://gitlab.com/GuilleW/disable-acpi-wakeup-from-usb.git
cd disable-acpi-wakeup-from-usb
sudo ./uninstall.sh
```
You can remove this folder after uninstall.
