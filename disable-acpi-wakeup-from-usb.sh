#!/bin/bash

declare devices=$(grep . /sys/bus/usb/devices/*/power/wakeup | grep enabled)
for device in "${devices[@]}"; do
  file=${device//:enabled/}
  if [ -f "$file" ]; then
    echo disabled > "${file}"
  fi
done
