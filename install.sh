#!/bin/bash

# Create service
cp ./disable-acpi-wakeup-from-usb.service /etc/systemd/system/disable-acpi-wakeup-from-usb.service
cp ./disable-acpi-wakeup-from-usb.sh /usr/local/bin/disable-acpi-wakeup-from-usb.sh
chmod +x /usr/local/bin/disable-acpi-wakeup-from-usb.sh

# Enable it
systemctl enable --now disable-acpi-wakeup-from-usb.service
