#!/bin/bash

# Disable it
systemctl disable disable-acpi-wakeup-from-usb.service

# Remove service
rm -f /etc/systemd/system/disable-acpi-wakeup-from-usb.service
rm -f /usr/local/bin/disable-acpi-wakeup-from-usb.sh
